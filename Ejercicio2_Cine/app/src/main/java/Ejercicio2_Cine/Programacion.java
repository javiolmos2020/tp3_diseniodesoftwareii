
package Ejercicio2_Cine;

import java.time.LocalDateTime;
import java.time.LocalTime;


public class Programacion {

    private LocalDateTime fechaDeInicio;
    private LocalDateTime fechaDeFin;
    private LocalTime horarioDeFunciones;
    private Pelicula pelicula;

    public Programacion(LocalDateTime fechaDeInicio, LocalDateTime fechaDeFin, LocalTime horarioDeFunciones,
            Pelicula pelicula) {

        this.fechaDeInicio = fechaDeInicio;
        this.fechaDeFin = fechaDeFin;
        this.horarioDeFunciones = horarioDeFunciones;
        this.pelicula = pelicula;

    }

    public LocalDateTime getFechaDeInicio() {

        return fechaDeInicio;

    }

    public void setFechaDeInicio(LocalDateTime fechaDeInicio) {

        this.fechaDeInicio = fechaDeInicio;

    }

    public LocalDateTime getFechaDeFin() {

        return fechaDeFin;

    }

    public void setFechaDeFin(LocalDateTime fechaDeFin) {

        this.fechaDeFin = fechaDeFin;

    }

    public LocalTime getHorarioDeFunciones() {

        return horarioDeFunciones;

    }

    public void setHorarioDeFunciones(LocalTime horarioDeFunciones) {

        this.horarioDeFunciones = horarioDeFunciones;

    }

    public Pelicula getPelicula() {

        return pelicula;

    }

    public void setPelicula(Pelicula pelicula) {

        this.pelicula = pelicula;

    }

}

