
package Ejercicio2_Cine;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Funcion {
    private Pelicula pelicula;
    private LocalDateTime fechaInicio;
    private LocalDateTime fechaFin;
    private int hora;
    private BigDecimal precio;

    public Funcion(Pelicula pelicula, LocalDateTime fechaInicio, int hora) {
        this.pelicula = pelicula;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaInicio.plusMinutes(pelicula.getDuracion());
        this.hora = hora;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public LocalDateTime getFechaInicio() {
        return fechaInicio;
    }

    public LocalDateTime getFechaFin() {
        return fechaFin;
    }

    public int getHora() {
        return hora;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public void registrarEntrada(Entrada entrada) {
        // Aqu� se podr�a agregar l�gica para registrar la entrada
        // en una base de datos, por ejemplo.
        System.out.println("Entrada registrada: " + entrada);
    }

    @Override
    public String toString() {
        return "Funcion{" +
                "pelicula=" + pelicula +
                ", fechaInicio=" + fechaInicio +
                ", fechaFin=" + fechaFin +
                ", hora=" + hora +
                ", precio=" + precio +
                '}';
    }
}





