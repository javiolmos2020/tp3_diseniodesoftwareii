//Sujeto a correcciones

/*package Ejercicio2_Cine;

public class Ejecutable {

    public static void main(String[] args) {

        // Creamos las películas
        Pelicula pelicula1 = new Pelicula("Drama", "El Padrino", 175, 16, Arrays.asList("Marlon Brando", "Al Pacino"), "Francis Ford Coppola");
        Pelicula pelicula2 = new Pelicula("Comedia", "The Hangover", 100, 18, Arrays.asList("Bradley Cooper", "Ed Helms"), "Todd Phillips");

        // Creamos la programación con las películas
        List<Funcion> programacion = new ArrayList<>();
        programacion.add(new Funcion(pelicula1, LocalDate.of(2023, 5, 11), LocalDate.of(2023, 5, 20), LocalTime.of(18, 30)));
        programacion.add(new Funcion(pelicula2, LocalDate.of(2023, 5, 11), LocalDate.of(2023, 5, 20), LocalTime.of(21, 0)));

        // Creamos el cine con la programación
        Cine cine = new Cine("CineCity", programacion);

        // Creamos los espectadores
        Espectador espectador1 = new Espectador("Juan", 25, new BigDecimal("200.00"));
        Espectador espectador2 = new Espectador("Lucia", 17, new BigDecimal("80.00"));
        Espectador espectador3 = new Espectador("Pedro", 30, new BigDecimal("150.00"));

        // Vendemos entradas para la función 1
        cine.venderEntrada(espectador1, 1);
        cine.venderEntrada(espectador2, 1);
        cine.venderEntrada(espectador3, 1);

        // Vendemos entradas para la función 2
        cine.venderEntrada(espectador1, 2);
        cine.venderEntrada(espectador2, 2);
        cine.venderEntrada(espectador3, 2);
    }
}*/
