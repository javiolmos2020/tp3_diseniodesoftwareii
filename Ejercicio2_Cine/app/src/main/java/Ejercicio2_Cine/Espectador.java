/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2_Cine;

import java.math.BigDecimal;


public class Espectador extends Persona {
    private BigDecimal dineroEnBilletera;

    public Espectador(String nombre, int edad, BigDecimal dineroEnBilletera) {
        super(nombre, edad);
        this.dineroEnBilletera = dineroEnBilletera;
    }

    public BigDecimal getDineroEnBilletera() {
        return dineroEnBilletera;
    }

    public void setDineroEnBilletera(BigDecimal dineroEnBilletera) {
        this.dineroEnBilletera = dineroEnBilletera;
    }

    public boolean tieneDineroSuficiente(BigDecimal precioEntrada) {
        return this.dineroEnBilletera.compareTo(precioEntrada) >= 0;
    }

    @Override
    public String toString() {
        return "Espectador{" +
                "nombre='" + getNombre() + '\'' +
                ", edad=" + getEdad() +
                ", dineroEnBilletera=" + dineroEnBilletera +
                '}';
    }
}

