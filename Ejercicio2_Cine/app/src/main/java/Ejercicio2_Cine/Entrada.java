/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2_Cine;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Entrada {
    private static int contadorEntradas = 0;

    private int numeroTicket;
    private LocalDateTime fechaVenta;
    private int numFuncion;
    private String nombrePelicula;
    private LocalDateTime fechaFuncion;
    private BigDecimal precio;
    private String calificacion;

    public Entrada(int numFuncion, String nombrePelicula, LocalDateTime fechaFuncion, BigDecimal precio, String calificacion) {
        this.numeroTicket = ++contadorEntradas;
        this.fechaVenta = LocalDateTime.now();
        this.numFuncion = numFuncion;
        this.nombrePelicula = nombrePelicula;
        this.fechaFuncion = fechaFuncion;
        this.precio = precio;
        this.calificacion = calificacion;
    }

    public int getNumeroTicket() {
        return numeroTicket;
    }

    public LocalDateTime getFechaVenta() {
        return fechaVenta;
    }

    public int getNumFuncion() {
        return numFuncion;
    }

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public LocalDateTime getFechaFuncion() {
        return fechaFuncion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public String getCalificacion() {
        return calificacion;
    }

    @Override
    public String toString() {
        return "Entrada{" +
                "numeroTicket=" + numeroTicket +
                ", fechaVenta=" + fechaVenta +
                ", numFuncion=" + numFuncion +
                ", nombrePelicula='" + nombrePelicula + '\'' +
                ", fechaFuncion=" + fechaFuncion +
                ", precio=" + precio +
                ", calificacion='" + calificacion + '\'' +
                '}';
    }
}