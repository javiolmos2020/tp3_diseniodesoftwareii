package Ejercicio2_Cine;

/*import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;


//Sujeto a correcciones


public class Cine {
    private String nombre;
    private ArrayList<Funcion> funciones;
    private boolean[][] butacas;

    public Cine(String nombre) {
        this.nombre = nombre;
        this.funciones = new ArrayList<>();
        this.butacas = new boolean[8][10];
    }

    public String getNombre() {
        return nombre;
    }

    public ArrayList<Funcion> getFunciones() {
        return funciones;
    }

    public boolean[][] getButacas() {
        return butacas;
    }

    public void agregarFuncion(Funcion funcion) {
        this.funciones.add(funcion);
    }

    public boolean puedeVerPelicula(Pelicula pelicula, Espectador espectador) {
        return espectador.getEdad() >= pelicula.getEdadMinima();
    }

    public boolean tieneDineroSuficiente(Espectador espectador, double precio) {
        return espectador.getDineroEnLaBilletera() >= precio;
    }

    public boolean hayAsientosDisponibles() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 10; j++) {
                if (!butacas[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public int[] asignarAsiento() {
        int[] asiento = {-1, -1};
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 10; j++) {
                if (!butacas[i][j]) {
                    asiento[0] = i;
                    asiento[1] = j;
                    butacas[i][j] = true;
                    return asiento;
                }
            }
        }
        return asiento;
    }

    public double calcularPrecio(Pelicula pelicula) {
        double precio = 10.0;
        if (pelicula.getGenero().equals("ESTRENO")) {
            precio += 2.5;
        }
        if (pelicula.getDuracion() > 120) {
            precio += 2.5;
        }
        if (pelicula.getDirector().getEdad() < 30) {
            precio += 1.5;
        }
        return precio;
    }
public boolean venderEntrada(Espectador espectador, int hora) {
   
    // Obtener la funci�n correspondiente a la hora indicada
    Funcion funcion = null;
    for (Funcion f : funciones) {
        if (f.getHora() == hora && f.getFechaFin().isAfter(LocalDateTime.now())) {
            funcion = f;
            break;
        }
    }
    if (funcion == null) {
        System.out.println("No hay funciones disponibles para la hora indicada.");
        return false;
    }

    // Verificar que el espectador tenga suficiente dinero en la billetera
    BigDecimal precioEntrada = funcion.getPrecio();
    if (!espectador.tieneDineroSuficiente(precioEntrada)) {
        System.out.println("El espectador no tiene suficiente dinero para comprar la entrada.");
        return false;
    }

    // Verificar que el espectador tenga edad suficiente para ver la pel�cula
    Pelicula pelicula = funcion.getPelicula();
    if (espectador.getEdad() < pelicula.getEdadMinima()) {
        System.out.println("El espectador no tiene la edad suficiente para ver la pel�cula.");
        return false;
    }

    // Buscar una butaca disponible
    Butaca butaca = null;
    for (int i = 0; i < filas && butaca == null; i++) {
        for (int j = 0; j < columnas && butaca == null; j++) {
            if (!asientos[i][j].estaOcupado()) {
                butaca = asientos[i][j];
            }
        }
    }
    if (butaca == null) {
        System.out.println("No hay butacas disponibles para esta funci�n.");
        return false;
    }

    // Asignar la butaca al espectador y marcarla como ocupada
    butaca.asignarEspectador(espectador);

    // Generar el n�mero de ticket de venta y registrar la entrada
    int numeroTicket = generarNumeroTicket();
    LocalDateTime fechaVenta = LocalDateTime.now();
    Entrada entrada = new Entrada(numeroTicket, funcion, pelicula, fechaVenta, butaca, precioEntrada);
    funcion.registrarEntrada(entrada);
    System.out.println("Entrada vendida con �xito.");

    return true;
}

private int generarNumeroTicket() {
    return proximoNumeroTicket++;
}*/