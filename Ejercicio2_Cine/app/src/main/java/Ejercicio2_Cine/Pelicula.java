/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2_Cine;

public class Pelicula {
    private String titulo;
    private String genero;
    private int duracion;
    private int edadMinima;
    private String director;
    private String[] actoresPrincipales;

    public Pelicula(String titulo, String genero, int duracion, int edadMinima, String director, String[] actoresPrincipales) {
        this.titulo = titulo;
        this.genero = genero;
        this.duracion = duracion;
        this.edadMinima = edadMinima;
        this.director = director;
        this.actoresPrincipales = actoresPrincipales;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String[] getActoresPrincipales() {
        return actoresPrincipales;
    }

    public void setActoresPrincipales(String[] actoresPrincipales) {
        this.actoresPrincipales = actoresPrincipales;
    }
}