package Ejercicio2_Cine;

public class Actor extends Persona {
    private String nombreArtistico;

    public Actor(String nombre, Integer edad, String nombreArtistico) {
        super(nombre, edad);
        this.nombreArtistico = nombreArtistico;
    }

    public String getNombreArtistico() {
        return nombreArtistico;
    }

    public void setNombreArtistico(String nombreArtistico) {
        this.nombreArtistico = nombreArtistico;
    }
}

