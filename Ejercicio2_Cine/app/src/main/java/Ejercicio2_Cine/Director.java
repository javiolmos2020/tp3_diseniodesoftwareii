/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2_Cine;

public class Director extends Persona {

    private String[] peliculasDirigidas;

    public Director(String nombre, int edad, String[] peliculasDirigidas) {
        super(nombre, edad);
        this.peliculasDirigidas = peliculasDirigidas;
    }

    public String[] getPeliculasDirigidas() {
        return peliculasDirigidas;
    }

    public void setPeliculasDirigidas(String[] peliculasDirigidas) {
        this.peliculasDirigidas = peliculasDirigidas;
    }
}
