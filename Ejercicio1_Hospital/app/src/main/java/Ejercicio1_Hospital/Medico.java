package Ejercicio1_Hospital;


public class Medico extends PersonalSanitario {
    
    private String especialidad;
    
    public Medico(String especialidad, int númeroEmpleado, String nombre) {
        super(númeroEmpleado, nombre);
        this.especialidad = especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    
}
