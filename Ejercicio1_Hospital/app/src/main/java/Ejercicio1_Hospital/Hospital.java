package Ejercicio1_Hospital;

import java.util.ArrayList;


public class Hospital {
    private String nombre;
    private ArrayList<Paciente> pacientes;
    private ArrayList<Medico> medicos;
    private ArrayList<Enfermero> enfermeros;
    private ArrayList<HistoriaClinica> historiasClinicas;

    public Hospital(String nombre) {
        this.nombre = nombre;
        this.pacientes = new ArrayList<>();
        this.medicos = new ArrayList<>();
        this.enfermeros = new ArrayList<>();
        this.historiasClinicas = new ArrayList<>();
    }

    public void agregarPaciente(Paciente paciente) {
        pacientes.add(paciente);
    }

    public void agregarMedico(Medico medico) {
        medicos.add(medico);
    }

    public void agregarEnfermero(Enfermero enfermero) {
        enfermeros.add(enfermero);
    }

    public void agregarHistoriaClinica(HistoriaClinica historiaClinica) {
        historiasClinicas.add(historiaClinica);
    }

    public Paciente buscarPacientePorNombre(String nombre) {
        for (Paciente paciente : pacientes) {
            if (paciente.getNombre().equals(nombre)) {
                return paciente;
            }
        }
        return null;
    }

    public Medico buscarMedicoPorNombre(String nombre) {
        for (Medico medico : medicos) {
            if (medico.getNombre().equals(nombre)) {
                return medico;
            }
        }
        return null;
    }

    public Enfermero buscarEnfermeroPorNombre(String nombre) {
        for (Enfermero enfermero : enfermeros) {
            if (enfermero.getNombre().equals(nombre)) {
                return enfermero;
            }
        }
        return null;
    }

    public void eliminarPaciente(Paciente paciente) {
        pacientes.remove(paciente);
    }

    public void eliminarMedico(Medico medico) {
        medicos.remove(medico);
    }

    public void eliminarEnfermero(Enfermero enfermero) {
        enfermeros.remove(enfermero);
    }
    
    public int getCantidadMedicos() {
        return medicos.size();
    }

    public int getCantidadEnfermeros() {
        return enfermeros.size();
    }

    public int getCantidadHistoriasClinicas() {
        return historiasClinicas.size();
    }

    public String getNombre() {
        return nombre;
    }
}