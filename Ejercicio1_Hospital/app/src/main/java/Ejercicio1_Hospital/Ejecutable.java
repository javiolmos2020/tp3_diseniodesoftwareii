
package Ejercicio1_Hospital;

import java.util.ArrayList;


public class Ejecutable {
    
    public static void main(String[] args) {
     // Crear objetos de la clase Medico
        Medico medico1 = new Medico("Pediatria", 12345, "Juan Perez");
        Medico medico2 = new Medico("Cardiolog�a",67890, "Ana Gomez");

        // Crear objetos de la clase Enfermero
        Enfermero enfermero1 = new Enfermero("Cuidados intensivos", 13579, "Pedro Ramirez");
        Enfermero enfermero2 = new Enfermero("Pediatr�a", 24680, "Laura Rodriguez");

        // Crear objeto de la clase Hospital
        Hospital hospital = new Hospital("Hospital ABC");

        // Agregar m�dicos y enfermeros al hospital
        hospital.agregarMedico(medico1);
        hospital.agregarMedico(medico2);
        hospital.agregarEnfermero(enfermero1);
        hospital.agregarEnfermero(enfermero2);

        
        HistoriaClinica historiaClinica1 = new HistoriaClinica(1, new ArrayList<Consulta>(), new ArrayList<Analisis>());
        historiaClinica1.agregarMedico(medico1);
        historiaClinica1.agregarEnfermero(enfermero1);

        HistoriaClinica historiaClinica2 = new HistoriaClinica(2, new ArrayList<Consulta>(), new ArrayList<Analisis>());
        historiaClinica2.agregarMedico(medico2);
        historiaClinica2.agregarEnfermero(enfermero2);

        // Agregar historias cl�nicas al hospital
        hospital.agregarHistoriaClinica(historiaClinica1);
        hospital.agregarHistoriaClinica(historiaClinica2);

        // Imprimir informaci�n del hospital
        System.out.println("Informaci�n del hospital: " + hospital.getNombre());
        System.out.println("Cantidad de m�dicos: " + hospital.getCantidadMedicos());
        System.out.println("Cantidad de enfermeros: " + hospital.getCantidadEnfermeros());
        System.out.println("Cantidad de historias cl�nicas: " + hospital.getCantidadHistoriasClinicas());
        System.out.println();
    }
}
