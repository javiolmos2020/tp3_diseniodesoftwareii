package Ejercicio1_Hospital;

import java.util.ArrayList;
import java.util.Date;

public class Paciente {

    private String nombre;
    private String apellido;
    private Date fechaDeNacimiento;
    private Integer identificacionPaciente;
    private String domicilio;
    private HistoriaClinica historiaClinica;
    private ArrayList<Consulta> consultas;
    private ArrayList<Analisis> analisis;

    public Paciente(String nombre, String apellido, Date fechaDeNacimiento, Integer identificacionPaciente, String domicilio, HistoriaClinica historiaClinica, ArrayList<Consulta> consultas, ArrayList<Analisis> analisis) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.identificacionPaciente = identificacionPaciente;
        this.domicilio = domicilio;
        this.historiaClinica = historiaClinica;
        this.consultas = consultas;
        this.analisis = analisis;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public Integer getIdentificacionPaciente() {
        return identificacionPaciente;
    }

    public void setIdentificacionPaciente(Integer identificacionPaciente) {
        this.identificacionPaciente = identificacionPaciente;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public HistoriaClinica getHistoriaClinica() {
        return historiaClinica;
    }

    public void setHistoriaClinica(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

    public ArrayList<Consulta> getConsultas() {
        return consultas;
    }

    public void setConsultas(ArrayList<Consulta> consultas) {
        this.consultas = consultas;
    }

    public ArrayList<Analisis> getAnalisis() {
        return analisis;
    }

    public void setAnalisis(ArrayList<Analisis> analisis) {
        this.analisis = analisis;
    }

    public void agregarConsulta(Consulta consulta) {
        this.consultas.add(consulta);
        this.historiaClinica.getConsultas().add(consulta);
    }

    public void agregarAnalisis(Analisis analisis) {
        this.analisis.add(analisis);
        this.historiaClinica.getAnalisis().add(analisis);
    }
    
  

}
