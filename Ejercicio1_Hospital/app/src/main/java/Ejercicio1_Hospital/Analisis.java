package Ejercicio1_Hospital;

import java.util.Date;


public class Analisis {
    private int numeroReferencia;
    private String tipo;
    private Date fecha;
    private Medico medico;
    private String resultado;

    public Analisis(int numeroReferencia, String tipo, Date fecha, Medico medico, String resultado) {
        this.numeroReferencia = numeroReferencia;
        this.tipo = tipo;
        this.fecha = fecha;
        this.medico = medico;
        this.resultado = resultado;
    }

    public int getNumeroReferencia() {
        return numeroReferencia;
    }

    public void setNumeroReferencia(int numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

  
    
    
}
