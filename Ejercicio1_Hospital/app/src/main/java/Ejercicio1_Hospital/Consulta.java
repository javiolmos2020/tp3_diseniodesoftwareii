package Ejercicio1_Hospital;

import java.util.Date;

public class Consulta {

    private Paciente paciente;
    private Date fecha;
    private Medico medico;
    private Enfermero enfermero;

    public Consulta(Paciente paciente, Date fecha, Medico medico, Enfermero enfermero) {
        this.paciente = paciente;
        this.fecha = fecha;
        this.medico = medico;
        this.enfermero = enfermero;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Enfermero getEnfermero() {
        return enfermero;
    }

    public void setEnfermero(Enfermero enfermero) {
        this.enfermero = enfermero;
    }

}
