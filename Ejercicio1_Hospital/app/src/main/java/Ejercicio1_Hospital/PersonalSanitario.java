
package Ejercicio1_Hospital;

public class PersonalSanitario {
    
    private int númeroEmpleado;
    private String nombre;

    public PersonalSanitario(int númeroEmpleado, String nombre) {
        this.númeroEmpleado = númeroEmpleado;
        this.nombre = nombre;
    }

    public int getNúmeroEmpleado() {
        return númeroEmpleado;
    }

    public void setNúmeroEmpleado(int númeroEmpleado) {
        this.númeroEmpleado = númeroEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
    
}
