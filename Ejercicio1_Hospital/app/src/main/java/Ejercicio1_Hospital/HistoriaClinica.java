package Ejercicio1_Hospital;

import java.util.ArrayList;

public class HistoriaClinica {

   private int numero;
    private ArrayList<Consulta> consultas;
    private ArrayList<Analisis> analisis;
    private ArrayList<Medico> medicos;
    private ArrayList<Enfermero> enfermeros;

    public HistoriaClinica(int numero, ArrayList<Consulta> consultas, ArrayList<Analisis> analisis) {
        this.numero = numero;
        this.consultas = consultas;
        this.analisis = analisis;
        this.medicos = new ArrayList<>();
        this.enfermeros = new ArrayList<>();
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public ArrayList<Consulta> getConsultas() {
        return consultas;
    }

    public void setConsultas(ArrayList<Consulta> consultas) {
        this.consultas = consultas;
    }

    public ArrayList<Analisis> getAnalisis() {
        return analisis;
    }

    public void setAnalisis(ArrayList<Analisis> analisis) {
        this.analisis = analisis;
    }

    public ArrayList<Medico> getMedicos() {
        return medicos;
    }

    public void setMedicos(ArrayList<Medico> medicos) {
        this.medicos = medicos;
    }

    public ArrayList<Enfermero> getEnfermeros() {
        return enfermeros;
    }

    public void setEnfermeros(ArrayList<Enfermero> enfermeros) {
        this.enfermeros = enfermeros;
    }

   public void agregarMedico(Medico medico) {
        this.medicos.add(medico);
    }

    public void agregarEnfermero(Enfermero enfermero) {
        this.enfermeros.add(enfermero);
    }


}
