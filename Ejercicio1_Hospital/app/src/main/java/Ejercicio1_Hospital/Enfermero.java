package Ejercicio1_Hospital;


public class Enfermero extends PersonalSanitario{
    private String especialidad;

    public Enfermero(String especialidad, int númeroEmpleado, String nombre) {
        super(númeroEmpleado, nombre);
        this.especialidad = especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    
}
